import github.io.wreed12345.Bot;

import java.net.URL;

public class Processor {
	
	private static boolean isChatting = false;
	
	private static ChatterBotFactory factory = new ChatterBotFactory();
	
	private static ChatterBotSession chatterbotSession;
	
	public static int numberOfStarbucks;
	
	public static String processString(Bot bot, String str, String user) throws Exception
	{
		str = str.toLowerCase();
		if (!user.equals("Bebot") && !isChatting)
		{
			if (str.indexOf("!k") != -1)
			{
				System.out.println(user + " issued command: !k");
				return "KKKK                        KKKK\nKKKK                    KKKK\nKKKK                KKKK\nKKKK            KKKK\nKKKK        KKKK\nKKKK    KKKK\nKKKKKKKKK\nKKKKKKK\nKKKKKKKKK\nKKKK    KKKK\nKKKK        KKKK\nKKKK            KKKK\nKKKK                KKKK\nKKKK                    KKKK\nKKKK                       KKKK";
			} else if (str.indexOf("!dstatus") != -1)
			{
				System.out.println(user + " issued command: !dstatus");
				return "Dominick's not in his room.";
			} else if (user.toLowerCase().indexOf("zach thompson") != -1)
			{
				System.out.println(user + " said something");
				return randomZach();
			} else if (user.toLowerCase().indexOf("dominick lee") != -1)
			{
				System.out.println(user + " issued command: dominick said something");
				if ((int)(Math.round(Math.random() * 5)) == 1) return "Ok Dominick.";
			} else if (str.indexOf("!starbucks") != -1)
			{
				System.out.println(user + " issued command: !bstar");
				return "Brandon owes Brian " + numberOfStarbucks + " Starbucks'";
			} else if (str.indexOf("!source") != -1)
			{
				System.out.println(user + " issued command: !source");
				return "Find the source for this bot at https://bitbucket.org/inb4ohnoes/bebot/overview";
			} else if (str.indexOf("!help") != -1)
			{
				System.out.println(user + " issued command: !help");
				return "Available commands:\n- !k\n- !dstatus\n- !help\n- !chathelp\n- !about\n- !source";
			} else if (str.indexOf("!chathelp") != -1)
			{
				System.out.println(user + " issued command: !chathelp");
				return "Bebot has a human chat feature.\nTo start talking to Bebot, simply say \"Hey Bebot\".\nTo top talking to Bebot, say \"Shut up Bebot\".";
			} else if (str.indexOf("!about") != -1)
			{
				System.out.println(user + " issued command: !about");
				return "Hey, I'm Bebot, a bot created by Brian Tung.\nIf you want more features, just talk to Brian and he'll probably be too lazy to implement it. " + randomEnd();
			} else if (str.indexOf("hey bebot") != -1)
			{
				Processor.isChatting = true;
				//init chatterbot
				ChatterBot chatterbot = factory.create(ChatterBotType.CLEVERBOT);
				chatterbotSession = chatterbot.createSession();
				System.out.println("Started chatting");
			    return "Whatsup " + user;
			}
			else if (str.contains("what") && str.contains("dinner")) {
				System.out.println(user + " asked what's for dinner");
				FoodGrabber grabber = new FoodGrabber(new URL("http://www.housing.purdue.edu/Menus/ERHT/"));
				Menu menu = grabber.getFood();
				if (!menu.getMeals()[2].getIsServing()) {
					return "Look who's going to go hungry tonight";
				}
				StringBuilder out = new StringBuilder("Here's whats for dinner at Earhart:\n");
				for (MenuItem item : menu.getMeals()[2].getMenuItems()) {
					out.append(item.toString() + "\n");
				}
				return out.toString();
			}
		} else if (!user.equals("Bebot"))
		{
			if (str.indexOf("shut up bebot") != -1)
			{
				Processor.isChatting = false;
				chatterbotSession = null;
				System.out.println("Stopped chatting");
				return "Ok :(";
			} else
			{
				return chatterbotSession.think(str);
			}
		}
		return null;
	}
	
	public static String randomZach()
	{
		int rand = (int)Math.round(Math.random() * 2);
		if (rand == 0)
		{
			return "Zach pls.";
		} else if (rand == 1)
		{
			return "I love you Zach <3";
		} else if (rand == 2)
		{
			return "Lookin' zesty today Zach";
		}
		return null;
	}
	
	public static String randomEnd()
	{
		int rand = (int)(Math.round(Math.random() * 5));
		if (rand == 0)
		{
			return "Good day!";
		} else if (rand == 1)
		{
			return "Now go away, or I shall taunt you a second time!";
		} else if (rand == 2)
		{
			return "What is love?";
		} else if (rand == 3)
		{
			return "Wyh u do dis?";
		} else if (rand == 4)
		{
			return "Have You Ever Considered How Can Mirrors Be Real If Our Eyes Aren't Real";
		} else if (rand == 5)
		{
			return "Desu~";
		}
		return null;
	}
}
