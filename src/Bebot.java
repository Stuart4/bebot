import java.io.*;
import java.util.*;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import org.json.JSONException;
import org.json.JSONObject;

import github.io.wreed12345.Bot;


public class Bebot {
	
	private static Bot bot;
	
	private static int port;

	public static void main(String[] args) throws Exception
	{
		System.out.println("Bot options:\n0 - Bot\n1 - CSBot\n2 - BebotTest\n3 - The Peeps");
		
		Scanner scanner = new Scanner(System.in);
		int option = scanner.nextInt();
		scanner.close();
		
		port = 8800 + option;
		
		if (option == 0)
		{
			bot = new Bot("9e20cbc6eabc89b47810ea848a");
		} else if (option == 1)
		{
			bot = new Bot("f072ec3e0813c0dd1126566056");
		} else if (option == 2)
		{
			bot = new Bot("3771982dd7d57e27a184befff8");
		} else if (option == 3)
		{
			bot = new Bot("ab89b61ae718213083439f6f3a");
		}
		
		if (bot != null)
		{
			//bot.sendTextMessage("Bebot - starting up...");
			System.out.println("Bebot - starting up...");
			
			//init server
			HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
			server.createContext("/receive_message", new MessageHandler());
			server.createContext("/update_count", new CountHandler());
			server.setExecutor(null); // creates a default executor
			server.start();
			System.out.println("Server running on port " + port);
			
			//bot.sendTextMessage("Bebot - started up!");
			System.out.println("Bebot - started up!");
		} else
		{
			System.out.println("Bot is null! Exiting...");
			System.exit(0);
		}	}

	static class MessageHandler implements HttpHandler
	{
		public void handle(HttpExchange t) throws IOException
		{
			InputStream stream = t.getRequestBody();
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			String readStr = reader.readLine();
            
			JSONObject object = null;
			try {
				object = new JSONObject(readStr);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			String str = "";
			String user = "";
			
			try
			{
				str = object.getString("text");
				user = object.getString("name");
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			String sendStr = "";
			try {
				sendStr = Processor.processString(bot, str, user);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (sendStr != null) 
			{
				bot.sendTextMessage(sendStr);
				sendResponse(t, 200, "OK");
			}
		}
	}
	
	static class CountHandler implements HttpHandler
	{
		public void handle(HttpExchange t) throws IOException
		{
			int shouldSendCount = Integer.parseInt((String)t.getRequestHeaders().getFirst("shouldSendCount"));
			if (shouldSendCount == 0)
			{
				Processor.numberOfStarbucks = Integer.parseInt((String)t.getRequestHeaders().getFirst("count"));
				sendResponse(t, 200, "" + Processor.numberOfStarbucks);
			} else
			{
				sendResponse(t, 200, "" + Processor.numberOfStarbucks);
			}
		}
	}
	
	public static void sendResponse(HttpExchange t, int code, String response) throws IOException
	{
		t.sendResponseHeaders(code, response.length());
		OutputStream os = t.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}
}
